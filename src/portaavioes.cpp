#include "portaavioes.hpp"
#include "embarcacao.hpp"
#include <iostream>

using namespace std;

void portaavioes::setlocal(int posicaox, int posicaoy,char direcao){		//seta a posição do porta-aviões de acordo com o recebido pelo mapa


	if (direcao == 'd'){		//Se o pa estiver virado para a direita...

		for (int i = 0; i < 4; i++){
			this->linha[i] = posicaox;
			this->coluna[i] = posicaoy + i;
		}

	}
	else if(direcao == 'e'){			//se estiver para a esquerda...
		for (int i = 0; i < 4; i++){
			this->linha[i] = posicaox;
			this->coluna[i] = posicaoy - i;
		}
	}
	else{
		for (int i = 0; i < 4; i++){	//Se estiver para cima...
			this->linha[i] = posicaox - i;
			this->coluna[i] = posicaoy;
		}
	}
}

int portaavioes::ataque(int posicaox, int posicaoy){		//Faz o processo de comparar posições do ataque
	if(posicaox == linha[0]+1 && posicaoy == coluna[0]+1){
			cout << "Você acertou um porta-aviões" << endl;
			parteabatida[0] = 1;
	}
	else if(posicaox == linha[1]+1 && posicaoy == coluna[1]+1){
			cout << "Você acertou um porta-aviões" << endl;
			parteabatida[1] = 1;
	}
	else if(posicaox == linha[2]+1 && posicaoy == coluna[2]+1){
			cout << "Você acertou um porta-aviões" << endl;
			parteabatida[2] = 1;
	}
	else if(posicaox == linha[3]+1 && posicaoy == coluna[3]+1){
			cout << "Você acertou um porta-aviões" << endl;
			parteabatida[3] = 1;
	}

	if(parteabatida[0] == 1 && parteabatida[1] == 1 && parteabatida[2] == 1 && parteabatida[3] == 1){ 	//Caso já houverem ataques em todas as posições...
			cout << "E você destruiu um porta-aviões" << endl;
			parteabatida[0] = 0;
			parteabatida[1] = 0;
			parteabatida[2] = 0;
			parteabatida[3] = 0;
			return 1;
		} 		
	else return 0;
}

int portaavioes::get_linha(int i){
	return linha[i];
}
int portaavioes::get_coluna(int i){
	return coluna[i];
}

portaavioes::portaavioes(){}
portaavioes::~portaavioes(){}