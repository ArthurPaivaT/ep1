#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include "embarcacao.hpp"
#include "submarino.hpp"
#include "canoa.hpp"
#include "portaavioes.hpp"
#include "mapa.hpp"

using namespace std;

void rodaojogo(){



	ifstream(leitordotxt);
	string linha;

	int posicaox[24], posicaoy[24];		//posições e direções que serão lidas do txt
	char direcao[24];
	int i = 0;


	leitordotxt.open("doc/map_1.txt");	//abre o txt 1 pra instanciar o mapa
	
	if(leitordotxt.is_open()){
		while(getline(leitordotxt, linha)){					//lê linha por linha do arquivo	
			
			if( linha[1] == ' '  && linha[0] >= 48 && linha[0] < 66) {	//caso o primeiro char da linha não esteja entre 48 e 66 na tabela ascii ele ignora
				posicaox[i] = linha[0] - 48 ;							//o primeiro inteiro é a posição x
				
				if(linha[3] == ' ') {									//caso o terceiro char seja um espaço...
					posicaoy[i] = linha[2] - 48;
					if (i < 6 || (i > 11 && i < 18) ) direcao[i] = linha[10];		
					else if (i < 10 || (i > 17 && i < 22)) direcao[i] = linha[14];
					else direcao[i] = linha[17];
				}
				else {
					if (i < 6 || (i > 11 && i < 18) )direcao[i] = linha[11];
					else if (i < 10 || (i > 17 && i < 21)) direcao[i] = linha[15];
					else direcao[i] = linha[18];
					posicaoy[i] =(linha[2] - 48)* 10 + linha[3] - 48;
				}

				//if ((i >= 0 && i < 6) || (i > 11 && i < 18) )	cout << linha[(10] << endl;
				//cout << direcao[i] << endl;
				i++;
			}
			else if (linha[1] != ' '  && linha[0] >= 48 && linha[0] < 66){
				posicaox[i] = (linha[0] - 48)*10 + linha[1] - 48;

				if(linha[4] == ' ') {
					if (i < 6 || (i > 11 && i < 18) )direcao[i] = linha[11];
					else if (i < 10 || (i > 17 && i < 21)) direcao[i] = linha[15];
					else direcao[i] = linha[18];
					posicaoy[i] = linha[3] - 48;
				}
				else{
					if (i < 6 || (i > 11 && i < 18) )direcao[i] = linha[12];
					else if (i < 10 || (i > 17 && i < 22)) direcao[i] = linha[16];
					else direcao[i] = linha[19];
					posicaoy[i] = (linha[3] - 48)* 10 + linha[4] - 48;
				}
				//Arquivo txt lido

				i++;
			}
			
		}

	}
	else cout << "Erro ao abrir o arquivo" << endl;

	//Cria os objetos canoas e seta suas posições
	
	canoa can1, can2, can3, can4, can5, can6, can7, can8, can9, can10, can11, can12;
	can1.setlocal(posicaox[0], posicaoy[0]);
	can2.setlocal(posicaox[1], posicaoy[1]);
	can3.setlocal(posicaox[2], posicaoy[2]);
	can4.setlocal(posicaox[3], posicaoy[3]);
	can5.setlocal(posicaox[4], posicaoy[4]);
	can6.setlocal(posicaox[5], posicaoy[5]);
	can7.setlocal(posicaox[12], posicaoy[12]);
	can8.setlocal(posicaox[13], posicaoy[13]);
	can9.setlocal(posicaox[14], posicaoy[14]);
	can10.setlocal(posicaox[15], posicaoy[15]);
	can11.setlocal(posicaox[16], posicaoy[16]);
	can12.setlocal(posicaox[17], posicaoy[17]);

	//Cria os objetos submarinos e seta suas posições
	submarino sub1, sub2, sub3, sub4, sub5, sub6, sub7, sub8;
	sub1.setlocal(posicaox[6],posicaoy[6],direcao[7]);
	sub2.setlocal(posicaox[7],posicaoy[7],direcao[7]);
	sub3.setlocal(posicaox[8],posicaoy[8],direcao[8]);
	sub4.setlocal(posicaox[9],posicaoy[9],direcao[9]);
	sub5.setlocal(posicaox[18],posicaoy[18],direcao[18]);
	sub6.setlocal(posicaox[19],posicaoy[19],direcao[19]);
	sub7.setlocal(posicaox[20],posicaoy[20],direcao[20]);
	sub8.setlocal(posicaox[21],posicaoy[21],direcao[21]);

	//Cria os objetos porta-aviões e seta suas posições
	portaavioes pa1, pa2, pa3, pa4;
	pa1.setlocal(posicaox[10], posicaoy[10], direcao[10]);
	pa2.setlocal(posicaox[11], posicaoy[11], direcao[11]);
	pa3.setlocal(posicaox[22], posicaoy[22], direcao[22]);
	pa4.setlocal(posicaox[23], posicaoy[23], direcao[23]);

	int q = 0;

	//Cria os objetos mapas e seta os símbolos iniciais como "^"
	mapa map1, map2;

	for(int n = 0; n < 13; n++){
		for(int m = 0; m < 13; m++){
			map1.setsimbolo(n, m, 1);
			map2.setsimbolo(n, m, 1);
		}
	}

	//Abre cada objeto//embarcação e seta suas posições no mapa

	for (q = 0; q < 4; q++) map1.setsimbolo(pa1.get_linha(q), pa1.get_coluna(q));
	for (q = 0; q < 4; q++) map1.setsimbolo(pa2.get_linha(q), pa2.get_coluna(q));

	for (q = 0; q < 2; q++) map1.setsimbolo(sub1.get_linha(q), sub1.get_coluna(q));
	for (q = 0; q < 2; q++) map1.setsimbolo(sub2.get_linha(q), sub2.get_coluna(q));
	for (q = 0; q < 2; q++) map1.setsimbolo(sub3.get_linha(q), sub3.get_coluna(q));
	for (q = 0; q < 2; q++) map1.setsimbolo(sub4.get_linha(q), sub4.get_coluna(q));

	map1.setsimbolo(can1.get_linha(), can1.get_coluna());
	map1.setsimbolo(can2.get_linha(), can2.get_coluna());
	map1.setsimbolo(can3.get_linha(), can3.get_coluna());
	map1.setsimbolo(can4.get_linha(), can4.get_coluna());
	map1.setsimbolo(can5.get_linha(), can5.get_coluna());
	map1.setsimbolo(can6.get_linha(), can6.get_coluna());

	for (q = 0; q < 4; q++) map2.setsimbolo(pa3.get_linha(q), pa3.get_coluna(q));
	for (q = 0; q < 4; q++) map2.setsimbolo(pa4.get_linha(q), pa4.get_coluna(q));

	for (q = 0; q < 2; q++) map2.setsimbolo(sub5.get_linha(q), sub5.get_coluna(q));
	for (q = 0; q < 2; q++) map2.setsimbolo(sub6.get_linha(q), sub6.get_coluna(q));
	for (q = 0; q < 2; q++) map2.setsimbolo(sub7.get_linha(q), sub7.get_coluna(q));
	for (q = 0; q < 2; q++) map2.setsimbolo(sub8.get_linha(q), sub8.get_coluna(q));

	map2.setsimbolo(can7.get_linha(), can7.get_coluna());
	map2.setsimbolo(can8.get_linha(), can8.get_coluna());
	map2.setsimbolo(can9.get_linha(), can9.get_coluna());
	map2.setsimbolo(can10.get_linha(), can10.get_coluna());
	map2.setsimbolo(can11.get_linha(), can11.get_coluna());
	map2.setsimbolo(can12.get_linha(), can12.get_coluna());



	//Introdução ao jogo
	system ("clear");
	cout << "Bem vindo, o jogo a seguir é uma batalha naval do primeiro projeto de Orientação a Objetos!" << endl << endl;
	system("sleep 1");
	cout << "As posições ainda não atacadas serão mostradas como \"^\" " << endl;
	cout << "As posições atacadas que não tinham embarcação serão mostradas como \"~\" " << endl;
	cout << "As posições atacadas que tinham uma embarcação serão mostradas como \"x\" " << endl;
	system("sleep 9");
	cout << endl << "Vamos começar com o jogador 1" << endl;
	system("sleep 2");
	system("clear");

	int jogadax, jogaday;
	int resultado1 = 0, resultado2 = 0;

	//Main loop...

	while(resultado1 < 12 && resultado2 < 12){			//Enquanto alguém não destruir as doze embarcações do adversário
		system("clear");
		cout << "\t Vez do Jogador 1\n\n";
		map2.imprimeoculto();							//Printa o mapa que se vê do inimigo
		cout << "\nJogada do player 1:\n";
		cin >> jogadax >> jogaday;						//Lê o ataque
		map2.jogada(jogadax -1, jogaday -1);			//Seta o ataque no mapa e altera o char a ser printado naquela posição

		//Faz a comparação da posição do ataque com cada embarcação, caso houve um acerto o método retorna 1 e soma 1 ao resultado
		resultado1 += can7.ataque(jogadax, jogaday);
		resultado1 += can8.ataque(jogadax, jogaday);
		resultado1 += can9.ataque(jogadax, jogaday);
		resultado1 += can10.ataque(jogadax, jogaday);
		resultado1 += can11.ataque(jogadax, jogaday);
		resultado1 += can12.ataque(jogadax, jogaday);
		resultado1 += sub5.ataque(jogadax, jogaday);
		resultado1 += sub6.ataque(jogadax, jogaday);
		resultado1 += sub7.ataque(jogadax, jogaday);
		resultado1 += sub8.ataque(jogadax, jogaday);
		resultado1 += pa3.ataque(jogadax, jogaday);
		resultado1 += pa4.ataque(jogadax, jogaday);

		fflush(stdin);
    	cin.get(); 

    	cout  << "Passe para o próximo jogador e tecle enter\n";	
		fflush(stdin);
    	cin.get(); 
    	system("clear");

    	cout << "\t Vez do Jogador 2\n\n";
		map1.imprimeoculto();
		cout << "\n Jogada do player 2:\n";
		cin >> jogadax >> jogaday;
		map1.jogada(jogadax -1, jogaday -1);
		resultado2 = resultado2 + can1.ataque(jogadax, jogaday);

		//Faz o processo de comparação de posições do ataque e aumenta o valor de resultado caso o jogador 
		resultado2 += can1.ataque(jogadax, jogaday);
		resultado2 += can2.ataque(jogadax, jogaday);
		resultado2 += can3.ataque(jogadax, jogaday);
		resultado2 += can4.ataque(jogadax, jogaday);
		resultado2 += can5.ataque(jogadax, jogaday);
		resultado2 += can6.ataque(jogadax, jogaday);
		resultado2 += sub1.ataque(jogadax, jogaday);
		resultado2 += sub2.ataque(jogadax, jogaday);
		resultado2 += sub3.ataque(jogadax, jogaday);
		resultado2 += sub4.ataque(jogadax, jogaday);
		resultado2 += pa1.ataque(jogadax, jogaday);
		resultado2 += pa2.ataque(jogadax, jogaday);

		fflush(stdin);
    	cin.get(); 
		if(resultado1 < 12 && resultado2 < 12 )cout << "Passe para o próximo jogador e tecle enter\n";	//Só printa isso aqui antes do loop acabar se ngm tiver ganhado ainda
		fflush(stdin);
    	cin.get(); 
    	system("clear");

	}
	//Printa os resultados do jogo

	if(resultado2 == 13 && resultado1 < 13) cout << "O jogador 2 venceu o jogo!!" << endl << endl;
	else if (resultado1 == 13 && resultado2 < 13) cout << "O jogador 1 venceu o jogo!!" << endl << endl;
	else cout << "Houve um empate !!!!!!" << endl << endl;

	cout << "O jogador 1 destruiu " << resultado1 << " embarcações!" << endl;
	cout << "O jogador 2 destruiu " << resultado2 << " embarcações!" << endl;
	
}

int main(){

	
	rodaojogo();
	

return 0;
}
