#include "submarino.hpp"
#include "embarcacao.hpp"
#include <iostream>

using namespace std;

void submarino::setlocal(int posicaox, int posicaoy,char direcao){	//seta a posição do submarino de acordo com o recebido pelo mapa

	if (direcao == 'd'){		//Se o submarino estiver virado para a direita...
		this->linha[0] = posicaox;
		this->linha[1] = posicaox;
		this->coluna[0] = posicaoy;
		this->coluna[1] = posicaoy + 1;

	}
	else if(direcao == 'e'){	//se estiver para a esquerda...
		this->linha[0] = posicaox;
		this->linha[1] = posicaox;
		this->coluna[0] = posicaoy;
		this->coluna[1] = posicaoy - 1;
	}
	else{						//Se estiver para cima...
		this->linha[0] = posicaox;
		this->linha[1] = posicaox - 1;
		this->coluna[0] = posicaoy;
		this->coluna[1] = posicaoy;
	}
}

int submarino::ataque(int posicaox, int posicaoy){		//Faz o processo de comparação de posições com o araque do jogador
		if(posicaox == linha[0]+1 && posicaoy == coluna[0]+1){
			cout << "Você acertou um submarino" << endl;
			parteabatida[0] = 1;
		}
		else if(posicaox == linha[1]+1 && posicaoy == coluna[1]+1){
			parteabatida[1] = 1;
			cout << "Você acertou um submarino" << endl;
		}
		if(parteabatida[0] == 1 && parteabatida[1] == 1){
			cout << "E você destruiu um submarino" << endl;
			parteabatida[0] = 0;
			parteabatida[1] = 0;
			return 1;
		} 		
		else return 0;
}

int submarino::get_linha(int i){
	return linha[i];
}
int submarino::get_coluna(int i){
	return coluna[i];
}

submarino::submarino(){}
submarino::~submarino(){}