#include "mapa.hpp"
#include <stdlib.h>
#include <iostream>

using namespace std;

void mapa::setsimbolo(int posicaox, int posicaoy){
	this->simbolo[posicaox][posicaoy] = 111;
}
void mapa::setsimbolo(int posicaox, int posicaoy, int i){	//Seta as posições de acordo com o ataque do jogador
	this->simbolo[posicaox][posicaoy] = 94;
	this->simbolooculto[posicaox][posicaoy] = 94;
}
char mapa::getsimbolo(int posicaox, int posicaoy){
	return simbolo[posicaox][posicaoy];
}

void mapa::jogada(int jogadax,int jogaday){
	if(simbolo[jogadax][jogaday] == 111){
		system("clear");
		this->simbolooculto[jogadax][jogaday] = 120;
	}
	else{
		system("clear");
		cout << "Nenhuma embarcação foi atingida\n";
		this->simbolooculto[jogadax][jogaday] = 126;
	}
}

void mapa::imprime(){					//Imprime o mapa da forma que o dono dele pode enchergar
	cout << "Mapa inimigo:\n";
	cout << "   A B C D E F G H I J K L M" << endl;

	for(int i = 0; i < 13; i++){
		if (i < 9){
			cout << " " << i+1;
		}
		else{
			cout << i+1;
		}

		for(int j = 0; j < 13; j++){
			cout << " " << simbolo[i][j];
		}
		cout << "\n";
	}
}

void mapa::imprimeoculto(){				//Imprime o mapa da forma que o adversário vê
	cout << "Mapa inimigo:\n";
	cout << "   A B C D E F G H I J K L M" << endl;

	for(int i = 0; i < 13; i++){
		if (i < 9){
			cout << " " << i+1;
		}
		else{
			cout << i+1;
		}

		for(int j = 0; j < 13; j++){
			cout << " " << simbolooculto[i][j];
		}
		cout << "\n";
	}
}

mapa::mapa(){}
mapa::~mapa(){}