#include "embarcacao.hpp"
#include <iostream>

using namespace std;

embarcacao::embarcacao(){
	tipo = "porta-avioes";
	posicao = 1;
	tamanho = 4;
	direcao = "direita";
}



embarcacao::~embarcacao(){
}

string embarcacao::get_tipo(){
	return tipo;
}
void embarcacao::set_tipo(string tipo_recebido){
	this->tipo = tipo_recebido;
}

int embarcacao::get_posicao(){
	return posicao;
}
void embarcacao::set_posicao(int posicao){
	this->posicao = posicao;
}

string embarcacao::get_direcao(){
	return direcao;
}
void embarcacao::set_direcao(string direcao){
	this->direcao = direcao;
}

int embarcacao::get_tamanho(){
	return tamanho;
}
void embarcacao::set_tamanho(int tamanho){
	this->tamanho = tamanho;
}

void embarcacao::alteratamanho(string tipo){
	if(tipo == "porta-avioes"){
		set_tamanho(4);
	}
	else if (tipo == "submarino"){
		set_tamanho(2);
	}
	else if (tipo == "canoa"){
		set_tamanho(1);
	}

}

