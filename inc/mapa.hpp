#ifndef MAPA_HPP
#define MAPA_HPP
#include <iostream>

using namespace std;

class mapa{
public:
	char simbolo[13][13];
	char simbolooculto[13][13];
	mapa();
	void setsimbolo(int posicaox,int posicaoy);
	void setsimbolo(int posicaox,int posicaoy, int i);
	char getsimbolo(int posicaox,int posicaoy);
	void jogada(int jogadax,int jogaday);
	void imprime();
	void imprimeoculto();
	~mapa();

};

#endif