#ifndef CANOA_HPP
#define CANOA_HPP

#include "embarcacao.hpp"
#include <string>

using namespace std;

class canoa:public embarcacao{
public:
	int linha, coluna;
	canoa();
	void setlocal(int linha, int coluna);
	int get_linha();
	int get_coluna();
	int ataque(int posicaox, int posicaoy);
	~canoa();

};

#endif