#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "embarcacao.hpp"
#include <string>

using namespace std;

class submarino:public embarcacao{
public:
	int linha[2], coluna[2], parteabatida[2];
	submarino();
	void setlocal(int linha, int coluna, char direcao);
	int get_linha(int i);
	int get_coluna(int i);
	int ataque(int posicaox, int posicay);
	~submarino();

};

#endif