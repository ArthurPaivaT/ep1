#ifndef PORTAAVIOES_HPP
#define PORTAAVIOES_HPP

#include "embarcacao.hpp"
#include <string>

using namespace std;

class portaavioes:public embarcacao{
public:
	int linha[4], coluna[4], parteabatida[4];
	portaavioes();
	void setlocal(int linha, int coluna, char direcao);
	int get_linha(int i);
	int get_coluna(int i);
	int ataque(int posicaox, int posicay);
	~portaavioes();

};

#endif