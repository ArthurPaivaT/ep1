#ifndef EMBARCACAO_HPP
#define EMBARCACAO_HPP

#include <string>

using namespace std;

class embarcacao {

private:
	
	string tipo;
	int posicao;
	string direcao;
	int tamanho;

public:

	embarcacao();
	~embarcacao();

	string get_tipo();
	void set_tipo(string tipo);

	int get_posicao();
	void set_posicao(int posicao);
	
	string get_direcao();
	void set_direcao(string direcao);

	int get_tamanho();
	void set_tamanho(int tamanho);

	void alteratamanho(string tipo);
	
};





#endif